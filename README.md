angular 1.5 video app

#install

npm i

#run

npm start

Based on Starter repo for Angular + ES6 + (Webpack or JSPM)
https://github.com/AngularClass/NG6-starter

#TODO

- common interface for all kinds video players with custom controls overlay
- handling errors on video request
- test...
