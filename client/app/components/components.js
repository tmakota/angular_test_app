import angular from 'angular';
import List from './list/list';
import videoPlayer from './video/html/video-player';
import youtubeAdapterPlayer from './video/youtube/youtube-adapter';
import facebook from './video/facebook/facebook';

let componentModule = angular.module('app.components', [
  List,
  videoPlayer,
  youtubeAdapterPlayer,
  facebook,
])
.name;

export default componentModule;
