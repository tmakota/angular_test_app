require('angular-sanitize')
import template from './video-player.html';
import controller from './video-player.controller';

let videoPlayerComponent = {
  bindings: {
    item: "<"
  },
  template,
  controllerAs: "video",
  controller,
};

export default videoPlayerComponent;
