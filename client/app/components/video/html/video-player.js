import angular from 'angular';
import videoPlayerComponent from './video-player.component';

let videoModule = angular.module('videoPlayer', [])
  .component('videoPlayer', videoPlayerComponent)
  .name;

export default videoModule;
