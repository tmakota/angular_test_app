import angular from 'angular';
import facebookComponent from './facebook.component';

let facebookModule = angular.module('facebook', [])
  .component('facebook', facebookComponent)
  .name;

export default facebookModule;
