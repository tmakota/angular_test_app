import template from './facebook.html';
import controller from './facebook.controller';

let facebookComponent = {
  bindings: {
    item: "<"
  },
  template,
  controllerAs: "facebook",
  controller,
};

export default facebookComponent;
