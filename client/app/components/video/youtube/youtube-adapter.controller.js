import youtubeVideo from 'angular-youtube-embed';

class youtubeAdapterController {

  constructor() {
    this.name = 'youtube';
  }

  youtubePlayerVars = {
    controls: 0,
    modestbranding: 1,
    showinfo: 0,
    rel: 0,
  };

}

export default youtubeAdapterController;
