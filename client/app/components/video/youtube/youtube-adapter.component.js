require('angular-sanitize');
import template from './youtube-adapter.html';
import controller from './youtube-adapter.controller';

let youtubeAdapterComponent = {
  bindings: {
    item: "<"
  },
  template,
  controllerAs: "youtube",
  controller,
};

export default youtubeAdapterComponent;
