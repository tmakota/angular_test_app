import angular from 'angular';
import youtubeAdapterComponent from './youtube-adapter.component';

let youtubeAdapterModule = angular.module('youtubeAdapter', [])
  .component('youtubeAdapter', youtubeAdapterComponent)
  .name;

export default youtubeAdapterModule;
