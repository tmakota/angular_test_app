import template from './list.html';
import controller from './list.controller';
import './list.scss';

let homeComponent = {
  bindings: {},
  template,
  controllerAs: "list",
  controller: ["$http", controller]
};

export default homeComponent;
