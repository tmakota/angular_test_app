import angular from 'angular';
import listComponent from './list.component';

let listModule = angular.module('list', [
      'youtube-embed',
   ])
  .component('list', listComponent)
  .name;

export default listModule;
