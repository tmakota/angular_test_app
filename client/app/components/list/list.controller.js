import fetchVideos from '../../services/fetch';
import { VIDEOS_URL } from '../../constants';

class ListController {

  constructor($http) {
    this.name = 'list';
    this.videos = [];
    this.http = $http;
  }

  $onInit = () => fetchVideos(this.http, VIDEOS_URL)
    .then(({ items }) => this.videos = items)
    .catch( err => err);

  yFilter = ({source}) => source === "youtube";

  urlsFilter = ({source}) => source === "url";

  fbFilter = ({source}) => source === "facebook";

}

export default ListController;
