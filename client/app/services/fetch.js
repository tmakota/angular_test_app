export default (http, url) => http.get(url).then( ( { data } ) => data);
