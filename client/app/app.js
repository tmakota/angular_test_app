import angular from 'angular';
import Common from './common/common';
import Components from './components/components';
import AppComponent from './app.component';
import 'normalize.css';

const app = angular.module('app', [
    Common,
    Components
  ])
  .component('app', AppComponent);

  app.config(function($sceProvider) {
      $sceProvider.enabled(false);
    });
